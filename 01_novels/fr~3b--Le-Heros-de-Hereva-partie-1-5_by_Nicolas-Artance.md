# Pepper et Carrot 3 - Le Héros de Hereva - Interlude 1.5
_Par Nicolas Artance, d'après l'univers d'Hereva de David Revoy ([http://www.peppercarrot.com/](http://www.peppercarrot.com/))_  

_Qualicity, château royal.  
Assis sur son trône, le roi de Qualicity regarde un écran dressé sur deux pieds, sur lequel sont projetés des images. Elles montrent un terrible géant de métal qui écrase une ville fortifiée, en détruisant tout autour de lui grâce à des rayons dévastateurs. Le château dressé au bord de la falaise menace de s'écrouler, mais il tient bon grâce à de puissantes racines, des toiles gélatineuses et un esprit de bras musclé, générés semble-t-il par... des jeunes filles. Ou plus vraisemblablement... des sorcières.  
L'une d'entre elles combat le géant, et le prive d'un de ses bras. Est-ce bien une sorcière ? Son aura ressemble plus à celle... d'un monstre. Elle génère une ouverture si puissante qu'elle balaie toute la ville et aspire ses habitants à l'intérieur. Ils sont cependant stoppés par une barrière invisible et retombent en douceur. Le géant, lui, et directement aspiré à l'intérieur du trou noir, tandis que le château est sauvé. Le roi observe tout ceci, le visage anxieux, jusqu'à ce que l'écran redevienne inerte._   

_**Roi Einsheneim :**_ "Est-ce que d'autres personnes ont vu ses images ?"  

_L'écran répond "non" de la tête._  

_**Roi Einsheneim :**_ "Et c'est heureux. Ce géant de métal... remontre-le-moi."  

_L'écran rembobine jusqu'à un plan suffisamment large du géant pour distinguer des symboles sur son corps. L'un d'entre eux attire l'attention du roi._  

_**Roi Einsheneim :**_ "Ses armoiries... Mère... il s'agissait donc bien..."  

_**Soumbala :**_ "Vraisemblablement, oui."  

_L'ancien Maître de Zombiah flotte, juste à côté du roi, assis en tailleur. Comme à son habitude, il semble en pleine méditation._  

_**Roi Einsheneim :**_ "Que sais-tu de lui ? Ma mère t'a-t-elle parlé de lui ?"  

_**Soumbala :**_ "Ma connaissance des travaux d'Apiacée est malheureusement limitée. Elle n'a inscrit dans ma mémoire que les éléments nécessaires à ma fonction. Tout ce que je peux dire, c'est que son journal ne parle en aucun cas de ce golem."  

_**Roi Einsheneim :**_ "Et pour cause... il faisait partie de ses travaux les plus secrets, dont elle refusait de parler à qui que ce soit, même aux membres les plus proches de sa famille. Quel tour de force d'avoir caché un tel géant à l'insu de tous..."

_**Soumbala :**_ "Si je puis me permettre, mon roi... si mon Créateur n'en a parlé à personne, comment ce fait-il que vous en ayez connaissance ?"  

_**Roi Einsheneim** (amusé) **:**_ "Tiens, tu poses des questions maintenant ? N'est-ce pas un peu bizarre de la part d'un sage ?"

_**Soubala :**_ "Le chemin de la connaissance ne cesse de s'étendre devant nous, peu importe le degré de sagesse. C'est un voyage éternel."

_**Roi Einsheneim :**_ "Euh... relax, hein, je plaisantais, c'est tout !"

_**Soumbala :**_ "Oh. Navré, ce doit être une routine de mon système, je ne peux m'empêcher d'explorer des réponses, même aux plaisanteries..."  

_**Roi Einsheneim :**_ "Ce n'est pas grave. En réalité... j'ai découvert l'existence de ce projet par hasard, alors que j'étais encore un enfant. J'ai ouvert une porte que je n'étais pas censé ouvrir, et je suis tombé sur ce golem, encore en construction. Je n'ai malheureusement pas pu en voir plus, car Mère m'a surpris et m'a emmené ailleurs, en me demandant de ne jamais rouvrir cette porte, ni de chercher à savoir ce qu'il contenait. Par la suite, je n'ai pas cessé de lui demander ce dont il s'agissait. Pour me calmer, elle m'a dit : "_Toutes les découvertes ont un prix, et il n'appartient qu'à moi de payer celle-ci. Reste prudent si tu veux t'engager sur la route de la connaissance._". C'est précisément ce qui m'a poussé à devenir ingénieur, moi aussi, et d'entammer ce fameux voyage éternel, comme tu l'appelles. Mais lorsque je fus enfin prêt à découvrir la vérité derrière ce projet... j'ai ouvert la porte, mais il n'y avait plus rien. Le géant n'était plus là. Je n'ai jamais su où il était, et ma mère étant décédée et ses grimoires vides, je n'en ai jamais su davantage... que faisait-il entre les mains de ce roi cupide et sans âme ? Était-ce sa fonction de... de tout détruire ? Non, ma mère n'aurait jamais conçu une telle arme... il devait protéger quelque chose, et ce satané roi Hartru le convoitait ! On doit découvrir ce que c'était..."  

_L'écran rediffuse la perte du bras du géant._  

_**Roi Einsheneim :**_ "Ce bras... montre-moi sa position actuelle !"  

_L'écran diffuse exactement le même endroit, sauf que le calme est revenu, et il ne reste que des gravas, et des gens qui essaient de les dégager. Mais aucune trace du bras._  

_**Roi Einsheneim :**_ "Le fourbe, il a dû le récupérer... fais envoyer des robots discrètement sur place, je veux savoir où est ce bras !"  

_Un arc électrique apparaît brièvement au-dessus de l'écran, entre ses deux antennes. Le message a l'air d'avoir été envoyé._  

_**Roi Einsheneim :**_ "C'est tout ce qu'il reste de ce golem... je veux savoir pourquoi ma mère a créé une telle chose."  

_**Soumbala :**_ "La vérité est souvent lourde de conséquences... êtes-vous prêt à les affronter ?"  

_**Roi Einsheneim :**_ "Encore une question ?"

_**Soumbala :**_ "Euh... est-ce une plaisanterie à nouveau ?"  

_**Roi Einsheneim :**_ "Non, je te fais marcher, mon bon ami ! Même si je ne t'ai jamais vu marcher, maintenant que j'y pense... je me demande pourquoi ma mère t'a créé des jambes, tu ne t'en sers jamais !"  

_**Soumbala :**_ "C'est aussi pour moi un mystère. Mais si elles existent, c'est qu'elles ont une mission à accomplir, une finalité à découvrir."  

_**Roi Einsheneim :**_ "Sans doute..."  

_L'écran montre à présent les remparts, où une jeune sorcière de Zombiah sort de l'inconscience._  

_**Roi Einsheneim :**_ "Ma Coriandre... j'espère qu'elle va bien..."  

_**Soumbala :**_ "Est-il prudent de la laisser prendre part à cette rébellion ? Elle est à présent une fugitive, passible d'emprisonnement."  

_**Roi Einsheneim :**_ "Elle est aussi tête brûlée que sa mère. Si elle a vu les armoiries, alors rien ne l'arrêtera pour découvrir la vérité. J'espère juste que... qu'elle ne subira pas le même sort funeste..."  

_**Soumbala :**_ "Le chemin de la connaissance est tortueux, il est facile de s'y égarer. Et en général, ceux qui s'y perdent ne retrouvent jamais le chemin de la vertu. Votre épouse en a malheureusement fait les frais malgré vos avertissements... mais votre fille dispose de la sagesse d'Apiacée. Je suis certain qu'elle s'efforcera de rester dans la lumière."  

_**Roi Einsheneim :**_ "Si seulement il y avait un moyen de s'en assurer..."  

_L'écran rediffuse l'aspiration des habitants vers le trou noir._  

_**Roi Einsheneim :**_ "Que penses-tu de ceci ? Les habitants auraient dû être aspirés, mais... il y a comme une barrière invisible qui les en empêche."  

_**Soumbala :**_ "Elle est sûrement générée par une des sorcières."  

_**Roi Einsheneim :**_ "Mais si tu observes bien..."  

_L'écran fige les images de toutes les sorcières à cet instant précis et les affiche dans des images séparées._  

_**Roi Einsheneim :**_ "...aucune d'entre elles semble le générer."  

_**Soumbala :**_ "Un intervenant extérieur... caché... avec suffisament de pouvoir pour créer une barrière imperméable à une force d'attraction aussi puissante... cela ne vous évoque rien ?"

_Le visage du roi se transforme en surprise lorsqu'il comprend._  

_**Roi Einsheneim :**_ "La légendaire Raven ? Que ferait-elle ici ?"  

_**Soumbala :**_ "Peut-être n'est-elle pas l'individu dangereux qui nous a été décrit. Et peut-être qu'un autre individu est plus dangereux qu'on ne l'aurait pensé..."  

_La sorcière responsable du trou noir déchaîne sa puissance contre le golem sur l'écran._  

_**Roi Einsheneim :**_ "Pepper... on ne la reconnaît pas sur ces images. Elle qui est si gentille, si serviable... c'est difficile de penser qu'elle est la source de tout ce carnage."  

_**Soumbala :**_ "Des événements troublants l'entourent ces derniers temps. Tout d'abord la cible de feue Maître Wasabi, qui jusque-là était plutôt discrète... ensuite le drame de l'Arbre de Komona, et maintenant ceci... Unitah... cette jeune sorcière est plus qu'il n'y paraît."  

_**Une voix :**_ "Je vous le fais pas dire."  

_Le roi sursaute. Soumbala reste dans sa position de méditation, alors qu'une invitée surprise se trouve en face de lui et de son souverain.
Les vêtements en lambeaux, un balai à la main, les cheveux en bataille, Pepper se dévoile derrière l'écran, une aura sombre l'entourant. Ces yeux sont écarlates, et son air... inquiétant._  

_**Roi Einsheneim :**_ "C'est... c'est toi, Pepper ? Je ne t'avais pas reconnue... comment es-tu entrée ?"  

_**Pepper :**_ "Vous devriez faire réparer les fuites de votre toit, cher souverain..."  

_Pepper désigne un trou au plafond._  

_**Soumbala :**_ "Je vous ai déjà dit de vous occuper des problèmes immobiliers plus rapidement."  

_**Roi Einsheneim :**_ "J'allais m'en occuper !! En tout cas, Pepper, tu ne peux pas entrer dans les châteaux royaux ainsi. Je te demanderais donc de bien vouloir sortir, s'il te plaît. Cela ne te concerne pas."  

_**Pepper** (désignant l'écran) **:**_ "Oui, parce qu'il ne s'agit pas de moi, peut-être, sur ces images ? C'est vous qui n'êtes pas concernés. Vous ne devriez pas avoir ces images."  

_Pepper s'avance d'un air menaçant. Le roi recule d'un pas en s'efforçant de garder son calme._  

_**Pepper :**_ "C'est amusant... je venais ici pour essayer d'en savoir plus sur ce golem qui nous a attaquées, et qu'est-ce que j'apprends ? Que le père de mon ex-amie détient des images de moi... la roue de la trahison poursuit paisiblement son chemin, on dirait."  

_**Roi Einsheneim :**_ "E...ex-amie ? Que s'est-il passé ? Où sont tes amies d'Unitah ?"  

_Pepper se met soudainement à rire. Mais un rire froid, sans âme._

_**Pepper :**_ "Ha, ha ! Vous n'êtes pas au courant ? Vous auriez dû visualiser la suite ! Comme le moment où mes soi-disant "amies" me trahissent, et me bannissent de l'équipe que j'ai moi-même fondée ! Vous ne l'avez pas vu, ça ? Pourtant, votre chère petite Coriandre s'est bien prononcée en la faveur de cette mesure injuste !"  

_**Roi Einsheneim :**_ "Quoi ?! Impossible ! Jamais Coriandre ne t'aurait chassée du groupe !"  

_**Pepper :**_ "C'est pourtant ce qu'elle a fait ! Elle m'a planté un couteau en plein coeur, sans pitié, comme toutes les autres ! Elles ont rejoint le camp des lâches, le camp de cette brute de roi de Grala !"

_**Roi Einsheneim :**_ "Non... jamais elle ne se joindrait à lui !"

_**Pepper :**_ "Oh, rassurez-vous, ce n'est pas vraiment sa faute : elle s'est faite embobiner par mon vieux maître, qui a pactisé avec lui et fourni des sorts pour se protéger de la magie et l'aider dans sa stupide quête de pouvoir ! Unitah est tombée bien bas... quelque part, je suis ravie de ne plus en être ! Et dire qu'elles pensent toutes que je suis responsable de cette catastrophe ! Que je suis dangereuse... qu'en pensez-vous, votre Altesse ? Suis-je dangereuse ? Les sorcières doivent-elles renoncer à la magie ? Vous étiez au jugement, après tout... vous avez bien dû donner votre accord pour que cette stupide loi soit approuvée ?"

_**Soumbala :**_ "Faux."  

_Pepper jette un oeil agacé en direction du robot._  

_**Soumbala :**_ "Mon souverain et l'Impératrice de Zhongdong ont été les seuls à s'opposer à cette mesure."  

_**Roi Einsheneim :**_ "SOUMBALA ! Il est interdit de révéler ce genre d'information ! Et le contrat ?"  

_**Soumbala :**_ "Le contrat magique ne s'applique qu'aux âmes qui pénètrent l'Ascétribune. L'âme est un concept bien abstrait pour ma personne."  

_**Pepper** (surprise) **:**_ "Ah... vous... vous vous êtes opposés à cette loi ?"  

_**Roi Einsheneim :**_ "Je n'ai pas le droit de te répondre."  

_**Soumbala:**_ "Mon souverain trouve cette loi injustifiée, il n'a donc pas voté pour son application."  

_**Pepper :**_ "Je vois... vous ne vous êtes pas opposé plus que ça, vous avez simplement choisi de ne pas prendre part au vote... ça aurait contrarié votre petite Coriandre chérie, n'est-ce pas ? Ni d'un côté, ni de l'autre, comme tous les lâches !"  

_**Roi Einsheneim :**_ "Je... je n'ai jamais..."  

_Pepper plaque alors le roi contre le mur avec ses pouvoirs._  

_**Pepper :**_ "NE ME MENTEZ PAS !! Vous avez ces images, vous pouvez me faire chanter en les diffusant à n'importe qui ! Alors, mon cher souverain, dîtes-moi : si jamais l'un de vos gardes nous surprenait, là, maintenant, que feriez-vous ? Me livrerez-vous à lui, ou m'aideriez-vous à m'enfuir ? De quel côté êtes-vous vraiment ?"  

_Le roi, la respiration haletante, jette un bref coup d'oeil à Soumbala. Pepper capte ce signe. Au moment où Soumbala semble vouloir bouger, un amas de particules flottant dans les airs se dresse sur sa route._  

_**Pepper :**_ "Même pas en rêve, tas de ferraille."  

_**Roi Einsheneim :**_ "Mais... qu'est-ce que c'est ?! Une créature ?!"  

_**Pepper :**_ "Je savais que cela vous fascinerait, Altesse ! Pour votre information, il s'agit de Messy, le seul être sur cette planète en qui j'ai encore confiance, et qui soutient ma cause."  

_**Roi Einsheneim :**_ "Le seul ?! Il n'a même pas de forme physique ! Et ton chat alors ?"  

_Pepper baisse les yeux., troublée._  

_**Pepper:**_ "Ce satané chat... il m'a trahie aussi. Ils m'ont TOUS trahie ! C'est terminé, je ne fais plus confiance à personne. Je voulais simplement récupérer mon droit de pratiquer la magie, mais au final, qu'est-ce que j'ai eu ? La solitude et la frustration. Et il est hors de question que ça continue. Alors vous allez bien gentiment détruire ces images que vous avez, et je promets de ne rien vous faire subir. D'accord ?"  

_Le roi, toujours tremblant, ne répond pas. Soumbala s'est figé devant Messy, qui le toise de sa hauteur.
Avant que le roi ne puisse répondre, un petit crissement de roue se fait entendre. Pepper détourne la tête : l'écran a déployé des petites roues et s'est échappé à toute vitesse jusqu'à un passage dans le sol qui s'est refermé immédiatement._ 

_**Pepper :**_ "Non, NON !"  

_Elle se précipite versle passage, bien refermé. Son aura s'intesifie, et ses yeux brillent d'une lueur écarlate._  

_**Pepper :**_ "VOUS M'AVEZ BIEN EUE AVEC VOS PETITS TOURS DE ZOMBI !"  

_**Roi Einsheneim :**_ "Pepper, calme-toi ! Ce n'était pas moi !"  

_**Pepper :**_ "Et en plus VOUS OSEZ MENTIR ?! J'en ai assez ! Vous n'auriez pas dû essayer de me tromper !"  

_Elle prépare un sort explosif malgré les suppliques du roi._  

_**Soumbala :**_ "Pepper, écoute-moi ! Tu te trompes de cible ! Celui qui a proposé cette loi n'est nul autre que..."  

_Mais une puissante rafale l'empêche de finir sa phrase. Pepper en subit les effets, ce n'est donc pas elle qui en est responsable. La salle du trône s'assombrit brusquement. Tout devient chaotique : impossible de distinguer quoi que ce soit, tant la rafale d'énergie est intense. Aucun d'entre eux ne peut bouger.  
La rafale finit par s'arrêter. Le roi ouvre un oeil timide, et se rend compte qu'il n'est plus collé au mur par une force invisible. Il regarde autour de lui, et voit Soumbala, toujours au-dessus du sol, qui regarde aussi autour de lui._  

_**Roi Einsheneim :**_ "Soumbala ! Tu vas bien ?" 

_**Soumbala :**_ "Il semblerait que mes routines n'aient pas été endommagées."  

_**Roi Einsheneim :**_ "Bon... mais où est Pepper ? Et cette créature bizarre ?"  

_Soumbala ne peut que hausser les épaules.  
À des centaines de mètres sous le château, un petit écran continue de rouler le long d'un corridor qui semble oublié depuis des lustres. Il roule, il roule... puis disparaît. À sa place émerge un homme grand, mince, dans une blouse blanche. Il redresse sa longue frange flottante et gratte sa petite barbichette soutenue d'une moustache fine d'un air satisfait._  

_**Dr Sandakropp :**_ "Voilà qui fut fort instructif..."

_À des kilomètres du château, une rafale d'énergie noire survole Hereva à une vitesse inouïe. Elle finit par plonger au coeur d'une forêt sombre et brumeuse. Elle semble frapper le sol avec une force indescriptible, puis s'évapore comme si elle était entrée à l'intérieur. En son sein se trouve Pepper, désarçonnée, qui tombe à genoux en s'étouffant quelque peu._  

_**Pepper :**_ "Bon sang ! Quelle force ! Je ne savais pas qu'ils avaient un tel système de défense..."  

_**Voix :**_ "Ils n'en ont pas."  

_Pepper se redresse d'un coup. En face d'elle, Messy flotte en l'air. Il redescend très lentement vers une silhouette qui devient plus claire lorsque la mystérieuse brume diminue très rapidement. Un kimono noir et rouge, des cheveux noirs comme la nuit très épais, des bottes ressemblant à des pattes d'oiseaux géantes, et surtout ce masque, à mi-chemin entre un corbeau et un dragon. Bouche bée, Pepper n'oublierait jamais ce masque surplombé de plumes noires._  

_**Pepper : **_ Raven...  

_Raven ne répond pas. Elle se contente de l'observer, les mains jointes, dans un silence dérangé uniquement par le bruit du feuillage des arbres à peine visibles, seuls témoins que le temps ne s'est pas encore arrêté._


**À SUIVRE DANS LA PARTIE 2**