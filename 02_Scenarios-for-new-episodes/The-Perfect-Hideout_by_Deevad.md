Episode 25: The perfect hideout
===============================

**page 01**

- Large view on a exotic and sunny landscape looking alike a fantasy tropical India, far away from Pepper home. The place is beautiful: waterfall, orange/redish stones, butterflies, exotic flowers. A detail; the pale three white moons are almost aligned and subtly visible on the pure blue sky.
- Pepper and Carrot are in this landscape. Pepper is obviously in holidays eating an exotic dish served on a banana leaf and offered by beautiful tropical-elves in colorful dresses while Carrot is following a mini dragon-gecko on the trunk of a coconut tree. Pepper is dressed with summer clothes, a lightweight variation of her usual clothes but with little exotic addition here and there. 
- Suddently every elves are looking up surprised, multiple things are flying above their heads (but not visible in the panel) and this things cast shadows on their face. Pepper tries to hide under her Banana-leaf-dinner, Carrot joined her. They are very nervous.

**page 02**

- Bird view, three witches dressed in black are flying over the coconut trees.
- Front view on facial expression of Thym, Cumin, Cayenne. They are flying on broom stick angry, in a V formation a bit like a band of motobikers. They are looking at the floor.
- (a) Zoom on Cayenne face: she noticed something.
- (b) Bird view, next to the little group of elves we can see a Banana leaf trying to escape, but this unusual walking banana leaf as another detail that don't make doubt for Cayenne: it has a cat tail. Carrot's tail.
- (c) Rare close-up shot in history where Cayenne is almost smiling with satisfaction.
- Close-up shot on Pepper's face terrified now while Cayenne (far away in background and as a mini silhouette) is pointing her finger into Pepper's direction and is screaming something to Cumin and Thym.

**page 03**

- large action shot, choatic scene: Pepper and Carrot are running now pushing the group of elves, making falling a lot of food and precious things while the three witches of Chaosah are chasing them flying low on their broomstick in background.
- (thick separator to indicate a longer cut) Landscape: Winter, low light, grey and cold house of Pepper.
- Pepper is now sitting in front of the desk in the library with her exotic clothes scratched as after a long long ellipsed chasing scene. She is in front of a open book, a stack of book as in episode 24. She is forced to study and she dislikes the situation, Carrot too, he has the fur not well groomed. At the door, the three witches are moving away, Cayenne still look at Pepper and Carrot while closing the door.

**page 04**

- Large shot on a new landscape, this time Pepper is relaxing on a big vines or root in jungle, using them as a hammock, Carrot is using vines as tarzan and has fun.
- We skip directly to a large shot of chaos with Pepper and Carrot running, the witches on broom stick. Scene is almost similar to what we already saw, only clothes, and landscape differs.
- We skip directly to the exact same shot in the library. Clothes of Pepper differ as well as little ivy plants in her hair, vines, etc...

**page 05**

- (1/2) Large shot on a new landscape, it's a cave from a cliff at the sea-side Pepper is enjoying sea in bathing suit.
- (1/2) library shot with a Pepper still in bathing suit and wet of water, same for Carrot.
- (1/2) Pepper is now on a field of colorfull flowers.
- (1/2) Pepper and Carrot in library with flowers in hair.
- (1/4) Pepper zen with lotus
- (1/4) Pepper library lotus on head
- (1/4) Pepper with bagpack walking in mountains
- (1/4) Pepper library bagpack almost destroyed
- (1/8) Pepper library with sand
- (1/8) Pepper library with mud
- (1/8) Pepper library with green leaves
- (1/8) Pepper library with other flowers
- (1/8) Pepper library with bees around
- (1/8) Pepper library with dust
- (1/8) Pepper library with big tropical flowers
- Pepper library with water again ... but Pepper has now an idea and not similar facial expression.

**page 06**

- landscape with Coconut tree ; 3 witches (silhouettes of Thym/Cumin/Cayenne) are flying and find nothing
- landscape with sea side ; 3 witches are flying and find nothing
- (a) landscape with oasis in desert ; same ... nothing
- (b) landscape with waterfall ; same ... nothing
- Close up on the witches's faces; they are confused, this time they can't find Pepper!

**page 07**

- Close up on Pepper with her finger in front of her lips (as if she said "shhh! don't tell anyone") looking at Carrot, they are on a dark place but it feels like a warm ambiant, light by candle. They both smile
- Large shot from the frame of a windows made of stack of books, Pepper is relaxing on a long chair made of book, Carrot also, and they enjoy their time.
- Zoom back more: large angle on the library room with all empty book shelf and a little house made of books in the middle of the room... (the perfect hideout was in the library, Pepper turned it in a place where she feels like in holidays)

_~FIN~_
