The present
===========

_**Author:** Nicolas Artance <[nicolas.artance@gmail.com](mailto:nicolas.artance@gmail.com)> with the help of @craigmaloney_
_**License:** [Creative Commons Attribution 4.0 International (CC BY 4.0)](https://creativecommons.org/licenses/by/4.0/)_

## Page 1  

**Panel 1:** Large panel showing a kind of big temple (japanese style), surrounded by white mist. Impossible to say if this temple is outside or not.    

**Panel 2:** There is a large garden in the center of the temple, very bright, where white and intense light floats. There are also some witches wearing kind of kimonos, who waters some impressive flowers. Some of them are wearing a priestess robe, and are preying in front of a kind of statue (which looks like a ghost). The garden is surrounded by doors. We see two young witches walking in that garden.   
_Shichimi: Here, the garden..._  

**Panel 3:** Zoom on Shichimi and Saffron. Shichimi comments with the face of someone forced to do something. Saffron has a boring face.  
_Shichimi: This is here that witches of Ah tries to enter into communion with spirits, by intensive meditation._  
_Saffron: It seems to be boring..._  

## Page 2  

**Panel 4:** Shichimi opens a sliding door with strange symbols, upset. Saffron is surprised.  
_Shichimi: So, the visit is now finished, and I have work to do. You can tell Master Wasabi I did my job. Have a nice day._  
_Saffron: W... wait!_  

**Panel 5:** Shichimi enters her room, followed by Saffron.  
_Saffron: Your Master said you have to show me all the public parts of the Temple!_  
_Shichimi: The garden is the only public part. Good bye._  

**Panel 6:** Shichimi is in front of a cauldron and starts to manipulate some potions. In the background, we can see two strange potteries which diffuse a white smoke, a little coffee table with all the material to do some tea, a shelf with mystic objects (stones, statuettes, etc) and a thick carpet near to a wall.   

**Panel 7:** A wild Saffron appears from the left, and makes her jump.  
_Saffron: What are you doing, so?_  

**Panel 8:** Shichimi turns toward Saffron. Saffron is trying to see what's in the cauldron, but Shichimi hides it.  
_Shichimi: It is not your business!_  
_Saffron: Oh, come on! You didn't show me anything here, you can tell me what you are working on, at least! New potion?_  

**Panel 9:** Shichimi starts to be angry. Saffron is surprised again.  
_Shichimi: It is a personal project which doesn't concern you and needs my entire focus! So be nice and leave me alone!_  

## Page 3  

**Panel 10:** Shichimi, angry, mixes two potions. Saffron, with a grumpy face and crossed arms, starts to look around her.  

**Panel 11:** Shichimi tries to spill only one drop of the potion in the cauldron. Saffron goes out the panel, on the left.  

**Panel 12:** Saffron returns, with a big silver stone in the hands. Shichimi jumps again, and spill more than a single drop in the cauldron.  
_Saffron: Is that precious?_  

**Panel 13:**  
_Shichimi: Put it down! It is the Moon Stone my Master offered me for my entry to Ah! It is fragile!_  
_Saffron: Okay, okay!_  

**Panel 14:** Saffron looks around her. Shichimi joins her hands and tries to focus although Saffron is speaking.  
_Saffron: But how can you focus with all that smoke?! You don't know what a window is?!_  

**Panel 15:** Zoom on the background.  
_Saffron: How poor living conditions..._  
_Saffron: I understand why you never show the private parts to anyone! You should tell your Master about that, it is unacceptable. At Magmah, we..._   

## Page 4  

**Panel 16:** Shichimi is now imploring. Saffron is clearly offended, and crosses her arms again, pressing the Moon Stone on her chest, lifting her nose, frowing.   
_Shichimi: Saffron, it is really important! I need you to shut your mouth! Please!!! AND PUT THIS STONE DOWN!_  
_Saffron: Okay! My advice is nothing compared to your precious work, I understand! Go on, if it is so important..._  

**Panel 17:** Shichimi joins her hands again.   
_Shichimi: Thanks._  

**Panel 18:** A little spirit made of flames appears.  

**Panel 19:** The spirit generates little white flames on the cauldron.  

**Panel 20:** Saffron starts to laugh.  
_Saffron: That's what you call "flames"?! Ha, ha, ha! So pathetic!_  
_Shichimi: Sshh!_  

## Page 5  

**Panel 21:** Saffron prepares a fire spell with one hand. Shichimi is shocked.  
_Saffron: Look what real flames are!_  
_Shichimi: NO!!_  

**Panel 22:** Saffron launches the spell. The cauldron is touched. Shichimi is horrified.  

**Panel 23:** The little spirit is impressed by the flames and looks at them with wonder.  
_Shichimi: YOU RUIN MY ENTIRE WORK!! Are you happy?!_  
_Saffron: At least, you have time to finish the visit, now!_  

**Panel 24:** There is "knock knock" sound from the cauldron. Both witches are surprised.  

**Panel 25:** Shichimi turns toward the cauldron to check it. She uses one arm to prevent Saffron to see inside the cauldron.  
_Saffron: What was that sound?!_  
_Shichimi: Nothing! You should go, now!_  

## Page 6  

**Panel 26:** Offended again, Saffron looks at the stone she always holds in her hands while Shichimi is looking inside the cauldron.  
_Shichimi: And return this stone to its place!_

**Panel 27:** Saffron throws the Moon Stone in the air, behind her. Shichimi is panicked._  
_Saffron: At your service!_  

**Panel 28:** Shichimi generates a little cloud to catch the Moon Stone, while Saffron looks at the cauldron.  

**Panel 29:** Saffron looks into the cauldron. Shichimi is horrified.  
_Saffron: Oh!_  

**Panel 30:** Saffron takes a little dragon in her hands.  
_Saffron: That's the surprise you worked on for me, isn't it?_  
_Shichimi: DON'T T... the... surprise?_  

**Panel 31:** Saffron is leaving with the dragon on her arms, tickling its muzzle. The dragon roars of satisfaction. In the background, Shichimi holds out her hand, confused.  
_Saffron: And I thought you forgot my birthday and you didn't pay attention to me!_  
_Saffron: Thank you for this wonderful present! <in little> Much better than a simple stone..._  
_Shichimi: But..._  

## *FIN*
