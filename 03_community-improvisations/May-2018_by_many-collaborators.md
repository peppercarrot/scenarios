Experiment
==========

**Rules**: each participant writes one panel and tries to be consistent with previous panels. Then he designates the next participant. Describe the main action in the present tense. (for example: *Pepper is singing*, not *Pepper was singing*)

---

@valvin

We are in Pepper’s bedroom during the middle of the night. Pepper is deeply sleeping while Carrot is just finding something in a chest. Carrot is trying to not make noise. In his paw, he’s holding a costume that looks like a super hero costume which can hide him enough not to be recognizable.

---

@xHire

Carrot is half on his way out of the chest (one leg down), carefully holding the costume in his mouth, paws on edge of the chest. His head and eyes are turned towards Pepper’s bed—making sure she still sleeps. He’s cautious not to have anyone notice him.

---

@imsesaok

The metal button on the costume falls out and makes a clanging noise. Carrot freaks out and jumps into the air. The chest door hits the wall and makes a loud bang.

---

@Midgard

Pepper turns around and groans. Carrot looks at her anxiously. Will she wake up?

---

@popolon

Carrot tries to pull the clothes out of the room, but a metal part scratches the ground.

---

@craigmaloney

Pepper mutters something incomprehensible. Carrot freezes in his tracks.

---

@Jookia

Pepper pulls the sheets up over herself to block out noise. The coast is clear.

---

@Nartance

Carrot sighs deeply. If Pepper would discover the truth...
But a little noise disturbs his thought. Like metal scratching the floor. Again?!
Carrot turns his head towards the noise. And what he sees surprises him a lot.

Indeed, a little creature tries to pull off the metal button of his costume, with great difficulty. It looks like a little woman, with short and messy hair held by a pink ribbon, wearing a dress made of leafs and flowers. But that’s not her first appearance which surprises Carrot, not even her small size. It’s the little wings that are flapping very quickly on her back, as hard was the effort to take the button.
The creature suddenly stops pulling the button. She looks at Carrot slowly, and seems to be terrorized.

“M... Mister Cat!” she stammers, terrified, with large gestures. “It... it’s not what you think! I... I just wanted to... I like shiny things... but... you can keep it! Please, Mister Cat! Don’t eat me!”

But Carrot’s feline instinct makes him swing his tail with great appetite. He prepares to jump on the prey...

---

@deevad

...but Carrot is interrupted at the last moment: a big shadow appears on the mirror of the bedroom of Pepper, a terrifying one with red eyes.

“Huuuu, Master, what are you doing here?” says the little fairy wearing a dress made of leafs.

---

@xHire

The dark contour of the mysterious figure raises its hand, points a finger at the clothes and slowly utters: “That’s… that’s the *Cloak of Eternal Chaosah Power.* My aeon-long search has finally come to an end. **YES!** Now, give it to me! It’s **mine!**”

Both Carrot and the fairy gaze at the red-eyed shadow with frozen poses.

---

@RJQuiralta

Meanwhile not too far from the bedroom, just outside the window, Carrot’s friends (the three cats, please insert names here) are waiting anxiously, each with a costume of their own, puzzled about what has taken Carrot so long...

---

@TheFaico

Suddenly, in front of Carrot's friends (names proposed: Asparagus, Zucchini and Radish; officially: Moustache, Tigrette and Big White) a boiling fog appears just in the middle of the darkness.
From it, a new creature appears. It looks like the terrorific one with red-eye, but this creature looks older, with blue-eye and doesn't cause fear at all.
He is Fig Orange aka Figor (intended pun of Igor, recurring servant's name), servant of the other one. He notice the cats, that looks at him astonished.
Figor doesn't say a word, he's going to try to enter to the witch's house, his master forgot something important.

---

@zeograd

From within Pepper's bedroom, we can see the mysterious creature getting nearer of Carrot in foreground. Carrot is holding tight the costume while arching his back.
Carrot is getting ready for the confrontation, while the little fairy is hiding behind him, trembling and surprised by her master attitude.

In the background, Figor can be seen at the window, waving at his master to attract his attention. Figor seems desperate to have his master looking at him and horrified by the scene he's witnessing.
In Figor's hand, we distinguish a faintly shining small item.

---

@valvin

While Figor tries to attract his master attention he doesn't pay attention where he is walking. His feet hurt an object on the floor. 
He stumbles and the shining small item flies in the air.

**New rules :** three turns left

---

@deevad

Moustache and Big White take advantage of the situation to tackle Figor to the ground while Tigrette catches in the air the shiny metalic object with a long jump.

This short time was enough to distract the shadow creature of the mirror.

---

@CalimeroTeknik

Pepper wakes up and sits in her bed, grunting "Carrrooooott, what are y…"

Opening the eyes to see what in the world Carrot is up to at this hour, Pepper's voice dies out.
As some miserable yelping is heard from outside the window, she is met with the sight of a terrified forest fairy and of Carrot, who looks like Puss in Boots interrupted while changing clothes in his wardrobe.

They're not looking at her, but at some fog emanating from her mirror.

Pepper tries to look closer, but the room is dark, and her footing isn't steady as she's still quite sleepy…

---

@xHire

Tigrette breaks a windowpane, landing on Pepper’s back with the metal piece in her stretched paw right in front of Pepper’s face. Pepper recognises it immediately and gasps: “The lost amulet! … The coat!” The mysterious master gets startled and extends his foggy “hand” towards Carrot.

With a single jump, Pepper grabs the coat from Carrot, throws it on her back and slides the amulet in its metal button in front. “Dispareo!” she shouts, looking into master’s red eyes. Purple light of high intensity starts glowing from the coat, fills the room and further escapes through the window in a strong beam. All three figures – the master, his servant Figor and the fairy – scream while vanishing in particles.

“Phew, that was close!” relieves Pepper when it’s over.


    ~ ~
~ The End ~
  ~ Fin ~
     ~
